#include "benchmark.h"

Benchmark::Benchmark(bigNum (*f)(int, unsigned int), int bits, unsigned int iterations, int times)
{
    this->f = f;
    this->bits = bits;
    this->iterations = iterations;
    this->times = times;
}

double Benchmark::timeval_diff(struct timeval *a, struct timeval *b)
{
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

double Benchmark::exec()
{
    time = 0;
    gettimeofday(&t_start, NULL);
    for(int i=0; i<this->times; i++){
        //cout << "i" << endl;
        f(this->bits, this->iterations);
    }
    gettimeofday(&t_end, NULL);
    time = timeval_diff(&t_end, &t_start);

    return (time/this->times);
}
