#include "vigenere.h"

Vigenere::Vigenere(string alphabet)
{
    this->alphabet = alphabet;
}

string Vigenere::encrypt(string msg, string pass)
{
    for(int i=0; i<msg.size(); i++)
    {
        msg[i] = alphabet[(alphabet.find(msg[i]) + alphabet.find(pass[i%pass.size()])) % alphabet.size()];
    }

    return msg;
}

string Vigenere::decrypt(string msg, string pass)
{
    for(int i=0; i<msg.size(); i++)
    {
        msg[i] = alphabet[module((alphabet.find(msg[i]) - alphabet.find(pass[i%pass.size()])),alphabet.size())];
    }

    return msg;
}

int Vigenere::module(int m, int n)
{
    if(m < 0)return m + n *(((-1*m)/n)+1);
    return m-n*(m/n);
}
