#ifndef RSA_H
#define RSA_H

#include "utils.h"
#include "prime.h"
#include "random.h"
#include "file.h"

class DigitalSignature;

class File;

class RSA
{
public:
    friend class DigitalSignature;
    RSA(int);
    RSA(bigNum, bigNum, bigNum, bigNum);
    RSA(bigNum, bigNum, bigNum);
    bigNum encryptNumber(bigNum);
    bigNum decryptNumber(bigNum);
    bigNum decryptNumberChinese(bigNum c);
    string encryptMessage(string);
    string decryptMessage(string);
    string encrypt(string message, bool);
    string encrypt(File&);
    string decrypt(File&);
    string decrypt(string message, bool);

private:
    string alphabet;
    bigNum privateKey;
    bigNum publicKey;
    bigNum N;
    bigNum p;
    bigNum q;
    RSA * destinatary;

    bigNum start;
    bigNum end;
    int nb;
    unsigned int iterations;
    void generate_keys();
};

#endif // RSA_H
