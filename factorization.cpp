#include "factorization.h"


Factorization::Factorization()
{
}

bigNum Factorization::PollardP_1(bigNum n)
{
    bigNum am, m, a, d, e;

    int upperBound = 100000;
    while(true)
    {
        //Declare sieve array
        vector<bool> sv(upperBound, true);
        nums primes = Utils::sieve(sv);

        m = NTL::to_ZZ(1);
        for(int i=0;i<primes.size();i++)m *= primes[i];

        while(true)
        {
            a = Random::getRand(NTL::to_ZZ(2),n-1);
            d = Utils::gcd(a,n);

            if(d!=1)return d;

            am = Utils::expMod(a,m,n);
            e = Utils::gcd(am-1, n);
            if(e == 1){
                upperBound++;
                break;
            }else if(e == n){
                continue;
            }else if((e != n)  && (e!=1) && (d!=1)){
                return d;
            }
        }
        //cout << m << endl;
    }
    return NTL::to_ZZ(0);
}

nums_pair Factorization::successiveDivisions(bigNum n, nums & primes)
{
    nums_pair factors;

    num i = 0;
    while((i<primes.size()) && (primes[i]*primes[i] < n))
    {

        if(n % primes[i] == 0)
        {
            n /= primes[i];
            if(!factors.empty())
            {
                if(factors.back().first == primes[i])
                {
                    factors.back().second++;
                }else{
                    factors.push_back(make_pair(primes[i], 1));
                }
            }else{
                factors.push_back(make_pair(primes[i], 1));
            }

        }else{
            i++;
        }
    }
    return factors;
}

