#ifndef FILE_H
#define FILE_H

#include "utils.h"
#include "rsa.h"
#include <cmath>
#include <QtCore/QCoreApplication>
#include "QtGui/QImage"
#include "QtGui/QPixmap"
#include "QtGui/QColor"

class File
{
public:
    friend class RSA;
    File(string, int, int, bool);
    string path;
    QImage * image;
    QImage * iEnc;
    int x;
    int y;
    string encoded;
    string encrypted;
    string decrypted;
    string decoded;
    void encodePixels();
    void saveEncryptedImage();
    void decodePixels();
    void saveDecryptedImage();
};

#endif // FILE_H
