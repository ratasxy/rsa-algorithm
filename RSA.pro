#-------------------------------------------------
#
# Project created by QtCreator 2014-04-29T07:03:48
#
#-------------------------------------------------

QT       += core

#QT       -= gui

TARGET = RSA
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    rsa.cpp \
    utils.cpp \
    prime.cpp \
    random.cpp \
    factorization.cpp \
    digitalsignature.cpp \
    vigenere.cpp \
    benchmark.cpp \
    runBenchmarks.cpp \
    file.cpp \
    affine.cpp

HEADERS += \
    rsa.h \
    utils.h \
    prime.h \
    random.h \
    factorization.h \
    digitalsignature.h \
    vigenere.h \
    benchmark.h \
    file.h \
    affine.h

CONFIG += c++11

unix: LIBS += -lntl
