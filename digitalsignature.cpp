#include "digitalsignature.h"

DigitalSignature::DigitalSignature(int bits)
{
    this->bits = bits;
    this->alphabet = "abcdefghijklmnoqprstuvwxyz";

    int upperBound = 1000000;
    vector<bool> sv(upperBound, true);
    nums primes = Utils::sieve(sv);

    while(true){
    this->p = Prime::getPrimeNotStrong(bits, 80);

    nums_pair a = Factorization::successiveDivisions(p-1, primes);

    set<int> proof;
    bool isGenerator = false;
    int size = a.size();
    while(!isGenerator){
        if(proof.size() >= size)break;
        if(size==0)break;

        srand(Random::getSalt());
        int ran = rand();
        int r =  ran % size;

        if(proof.find(r) == proof.end()){
            proof.insert(r);
        }

        this->g = a[r].first;
        set<bigNum> nums;
        bigNum tmp;
        for(bigNum i=NTL::to_ZZ(1); i<this->p; i++)
        {
            tmp = Utils::expMod(this->g, i, this->p);
            if(nums.find(tmp) != nums.end()){
                break;
            }
            nums.insert(tmp);
        }
        cout << "Probando " << this->p << " con " << this->g << endl;
        if(nums.size() == this->p-1)isGenerator=true;
    }
    if(!isGenerator)continue;

    bigNum na = Random::getRand(NTL::to_ZZ(2), this->p-1);
    this->A = Utils::expMod(this->g,na,this->p);
    break;
    }

    this->vigenere = new Vigenere(this->alphabet);

    cout << endl << "Diffie-Hellman: " << endl;
    cout << "P: " << this->p << endl;
    cout << "G: " << this->g << endl;
    cout << "A: " << this->A << endl;
}

void DigitalSignature::preUse()
{
    bigNum a;
    bigNum start, end;
    start = NTL::to_ZZ(1) << bits;
    end = NTL::to_ZZ(1) << (bits >> 1);
    do{
        a = Random::getRand(start, end);
    }while(Utils::gcd(a, NTL::to_ZZ(this->alphabet.size())) != 1);


    bigNum b = Random::getRand(start, end);
    bigNum B = Utils::expMod(this->g, b, this->p);

    bigNum k = Utils::expMod(this->A, b, this->p);
    this->affine = new Affine(a,k, this->alphabet);
    cout << endl << "Enviar :" << endl;
    cout << "B: " << B << endl;
    cout << endl << "Privado :" << endl;
    cout << "a: " << a << endl;
    cout << "k: " << k << endl;
}

string DigitalSignature::sign(string message, string pass, RSA * rsa)
{
    string vEncrypted = this->vigenere->encrypt(message, pass);
    string aEncrypted = this->affine->encrypt(vEncrypted);

    vector<string> data = Utils::splitStrings(aEncrypted, this->alphabet);

    string text = Utils::join(data);

    vector<string> encryptedMessage;
    int sizeOfN = Utils::to_string(rsa->N).size();
    for(unsigned long long i = 0; i < text.size(); i+=(sizeOfN-1)){
        string txt = Utils::addZeros(text.substr(i, (sizeOfN-1)), sizeOfN-1, true);
        bigNum m = rsa->decryptNumberChinese(Utils::to_num(txt));
        encryptedMessage.push_back(Utils::addZeros(Utils::to_string(m), sizeOfN));
        //cout << "E: " << Utils::addZeros(Utils::to_string(m), sizeOfN) << endl;
    }

    text = Utils::join(encryptedMessage);

    return rsa->destinatary->encryptMessage(text);
}
