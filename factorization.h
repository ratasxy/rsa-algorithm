#ifndef FACTORIZATION_H
#define FACTORIZATION_H

#include "utils.h"
#include "random.h"

class Factorization
{
public:
    Factorization();
    static bigNum PollardP_1(bigNum);
    static nums_pair successiveDivisions(bigNum, nums &);
};

#endif // FACTORIZATION_H
