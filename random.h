#ifndef RANDOM_H
#define RANDOM_H

#include "utils.h"
#include <fstream>

class Random
{
public:
    Random();

    static int getSalt();
    static bigNum getRand(bigNum, bigNum);
    static int cryptoSysSalt();
};

#endif // RANDOM_H
