#include "utils.h"
#include "random.h"
#include <vector>
#include "factorization.h"

Utils::Utils()
{
}

bigNum Utils::gcd(bigNum u, bigNum v)
{
    int shift;

    if (u == 0) return v;
    if (v == 0) return u;

    for (shift = 0; ((u | v) & 1) == 0; ++shift) {
        u >>= 1;
        v >>= 1;
    }

    while ((u & 1) == 0)
    u >>= 1;

    do {
        while ((v & 1) == 0)
            v >>= 1;

        if (u > v) {
            bigNum t = v; v = u; u = t;}
            v = v - u;
     } while (v != 0);

    return u << shift;
}



bigNum Utils::expMod(bigNum b, bigNum e, const bigNum m)
{
    bigNum r = NTL::to_ZZ(1);

    while (e > 0 && b != 0) {
        if (NTL::IsOdd(e)) r = r * b % m;
        b = b * b % m;
        e >>= 1;
    }

    return r;
}

vector<bigNum> Utils::extendedGCD(bigNum n, bigNum m)
{
    bigNum a = n, b = m, q;
    bigNum u = NTL::to_ZZ(1);
    bigNum v = NTL::to_ZZ(0);
    do
    {
        q = a / b;

        const bigNum t1 = a - q*b;
        a = b;
        b = t1;

        const bigNum t2 = u - (bigNum)q*v;
        u = v;
        v = t2;
    } while (b != 0);
    if (a != 1) u = 0;
    if (u < 0) u += m;

    return {u, v , q};
}

bigNum Utils::inverse(bigNum n, bigNum m)
{
    return extendedGCD(n, m)[0];
}

bigNum Utils::chineseRemainder(bigNum a1, bigNum b1, bigNum a2, bigNum b2)
{
    bigNum q = b1 * b2;
    bigNum p1 = b2, p2 = b1, q1, q2;

    q1 = inverse(p1, p2);
    q2 = inverse(p2, p1);

    bigNum d = (((((a1*p1)%q)*q1)%q) + ((((a2*p2)%q)*q2)%q)) % q;

    return d;
}

bigNum Utils::to_num(string n)
{
    return NTL::to_ZZ(n.c_str());
}

string Utils::to_string(bigNum n)
{
    if (n == 0) return "0";
    string ns = "0123456789";
    string r;
    for(bigNum j = n % NTL::to_ZZ(10); n != 0 ; n /= 10, j = n % 10)
    {
        r.insert(0,1,ns[NTL::to_uint(j)]);
    }
    return r;
}

string Utils::addZeros(string message, int nZeros)
{
    int nToadd = nZeros - message.size();
    string add = "";
    if(nToadd < 0)
    {
        cout << "MSG:" << message << endl;
        cout << "nToadd: " << nToadd << endl;
        cout << "error" << endl;

    }else{
        add = string(nToadd, '0');
    }


    return add + message;
}

string Utils::addZeros(string message, int nZeros, bool reverse)
{
    int nToadd = nZeros - message.size();
    string add(nToadd, '0');

    return message + add;
}

vector<string> Utils::splitStrings(string message, string alphabet)
{
    vector<string> result;

    int size = Utils::to_string(NTL::to_ZZ(alphabet.size())).size();

    for(unsigned long long i = 0; i < message.size(); i++) {
        bigNum m = NTL::to_ZZ(alphabet.find(message[i]));

        result.push_back(Utils::addZeros(Utils::to_string(m), size));
    }

    return result;
}

string Utils::addNZeros(string message, int nZeros)
{
    string add(nZeros, '0');

    return add +  message;
}

string Utils::join(vector<string> items)
{
    string result;
    for(unsigned int i = 0; i < items.size(); i++)
    {
        result += items[i];
    }
    return result;
}

nums Utils::sieve(bools &sv)
{
    sv[0] = false;
    sv[1] = false;

    nums primes;
    for(bigNum i=NTL::to_ZZ(2); i*i<sv.size();i++)
    {
        if(sv[NTL::to_int(i)] == true){
            primes.push_back(i);
            for(bigNum j=NTL::to_ZZ(2); i*j<sv.size();j++)sv[NTL::to_int(i*j)] = false;
        }
    }

    return primes;
}

string Utils::encodeRGB(int red, int green, int blue)
{
    string r,g,b;

    r = Utils::addZeros(Utils::to_string(NTL::to_ZZ(red)), 3);
    g = Utils::addZeros(Utils::to_string(NTL::to_ZZ(green)), 3);
    b = Utils::addZeros(Utils::to_string(NTL::to_ZZ(blue)), 3);

    return r + g + b;
}

string Utils::inverseEncodeRGB(int red, int green, int blue)
{
    string r,g,b;

    r = Utils::addZeros(Utils::to_string(NTL::to_ZZ(red)), 2);
    g = Utils::addZeros(Utils::to_string(NTL::to_ZZ(green)), 2);
    b = Utils::addZeros(Utils::to_string(NTL::to_ZZ(blue)), 2);

    return r + g + b;
}
