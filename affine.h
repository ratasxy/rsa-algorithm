#ifndef AFFINE_H
#define AFFINE_H

#include <iostream>
#include <vector>
#include "utils.h"

using namespace std;

class DigitalSignature;

class Affine
{
  public:
  friend class DigitalSignature;
  string alphabet;
  bigNum a, b;

  Affine(bigNum, bigNum, string);
  string encrypt(string);
  string decrypt(string);

  private:
  int mod(bigNum, int);
};

#endif // AFFINE_H
