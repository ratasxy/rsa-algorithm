#ifndef DIGITALSIGNATURE_H
#define DIGITALSIGNATURE_H

#include "utils.h"
#include "prime.h"
#include "factorization.h"
#include "random"
#include "affine.h"
#include "vigenere.h"
#include "rsa.h"

class DigitalSignature
{
public:
    friend class RSA;
    int bits;
    bigNum p;
    bigNum g;
    bigNum A;
    string alphabet;
    Affine * affine;
    Vigenere * vigenere;

    DigitalSignature(int bits);
    void preUse();
    string sign(string message, string pass, RSA * rsa);
};

#endif // DIGITALSIGNATURE_H
