#ifndef VIGENERE_H
#define VIGENERE_H

#include <utils.h>

class Vigenere
{
public:
    Vigenere(string);
    string encrypt(string, string);
    string decrypt(string, string);
    int module(int, int);
private:
    string alphabet;
};

#endif // VIGENERE_H
