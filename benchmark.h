#ifndef BENCHMARK_H
#define BENCHMARK_H

#include "utils.h"
#include <sys/time.h>

class Benchmark
{
public:
    Benchmark(bigNum (*f)(int, unsigned int), int, unsigned int, int);
    struct timeval t_start, t_end;
    double time;
    bigNum (*f)(int, unsigned int);
    double exec();
    int bits;
    unsigned int iterations;
    int times;

private:
    double timeval_diff(struct timeval*, struct timeval*);
};

#endif // BENCHMARK_H
