#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <NTL/ZZ.h>
#include <set>

using namespace std;

typedef NTL::ZZ bigNum;
typedef vector<bool> bools;
typedef vector<bigNum> nums;
typedef vector<pair<bigNum, int> > nums_pair;
typedef unsigned long long num;

class Utils
{
public:
    Utils();
    static bigNum gcd(bigNum u, bigNum v);
    static vector<bigNum> extendedGCD(bigNum, bigNum);
    static bigNum expMod(bigNum b, bigNum e, const bigNum m);
    static bigNum inverse(bigNum n, bigNum m);
    static bigNum chineseRemainder(bigNum, bigNum, bigNum, bigNum);
    static string addZeros(string, int);
    static string to_string(bigNum);
    static bigNum to_num(string);
    static string join(vector<string>);
    static string addNZeros(string message, int nZeros);
    static string addZeros(string message, int nZeros, bool reverse);
    static vector<string> splitStrings(string, string);
    static nums sieve(bools &sv);
    static string encodeRGB(int, int, int);
    static string inverseEncodeRGB(int, int, int);

};

#endif // UTILS_H
