#include "prime.h"

Prime::Prime()
{
}

bool Prime::isPrime(bigNum w, unsigned int iterations = 50)
{
    if(!NTL::IsOdd(w))return 0;

    //Miller-Rabin Test
    srand(Random::getSalt());
    unsigned int a = 0;
    bigNum W = w - 1;
    bigNum m = W;
    while (bit(m, 1) != 1){
        m >>= 1;
        a++;
    }

    for(unsigned int i = 0; i < iterations; i++){
        bigNum b = (NTL::to_ZZ(rand() ) % (W - 3)) + 2;
        //cout << "Powermod 1 -> b: "<<b<< " m: " << m << " w: " << w << endl;
        bigNum z = NTL::PowerMod(b, m, w);
        if ((z == 1) || (z == W))
            continue;
        else{
            int continueOuter = 0;
            for(unsigned int j = 1; j < a; j++){
                //cout << "Powermod 2 -> <: "<<z<< " ntl2: " << 2 << " w: " << w << endl;
                z = NTL::PowerMod(z, NTL::to_ZZ(2), w);
                if (z == W)
                    continueOuter = 1;
                    break;
                if (z == 1)
                    return 0;
            }
            if (continueOuter) {continue;}
        }
        return 0;
    }
    return 1;
}

bigNum Prime::getPrime(bigNum start, bigNum end, unsigned int iterations)
{
    bigNum x = NTL::to_ZZ(1);
    int intentos = 0;
    for (int i = 0; true; i++) {
        bigNum n = Random::getRand(start, end);

        n = n + x % (end - start);
        x++;

        if (n < 0) n = n * -1;
        intentos++;
        if (isPrime(n, iterations)) {
            //cout << "Intentos: "<< i << endl;
            return n;
        }
    }
}

bigNum Prime::getPrimeNotStrong(int bits, unsigned int iterations)
{
    bigNum start, end;
    start = NTL::to_ZZ(1) << bits;
    end = NTL::to_ZZ(1) << (bits >> 1);

    return getPrime(start, end, iterations);
}

bigNum Prime::getStrogPrimeByWilliamsSchmid(int bits, unsigned int iterations)
{
    bigNum p2, pp, p, tmp, a, start, end, r;

    bits = bits >> 1;
    start = NTL::to_ZZ(1) << bits;
    end = NTL::to_ZZ(1) << (bits >> 1);

    do{
        p2 = getPrime(start, end, iterations);
        pp = getPrime(start, end, iterations);
    }while(p2 == pp);

    if(p2>pp){
        tmp = p2;
        p2 = pp;
        pp = tmp;
    }

    r = Utils::inverse(p2, pp);
    a = 2;

    do{
        //cout << "Probando con: " << a << endl;
        p = (4*a*p2*pp) + (4*r*p2) + 3;
        a += 2;
    }while(!isPrime(p, iterations));

    return p;
}

bigNum Prime::getStrogPrimeByGordon(int bits, unsigned int iterations)
{
    bigNum start, end, pp, p0, p2, p1, a2, a, p, tmp;

    bits = bits >> 1;
    start = NTL::to_ZZ(1) << bits;
    end = NTL::to_ZZ(1) << (bits >> 1);

    do{
        p2 = getPrime(start, end, iterations);
        pp = getPrime(start, end, iterations);
    }while(p2 == pp);

    if(p2>pp){
        tmp = p2;
        p2 = pp;
        pp = tmp;
    }

    a2 = 1;

    do{
        p1 = (a2*p2)+1;
        //cout << "Test: " << a2 << endl;
        a2++;
    }while(!isPrime(p1, iterations));

    p0 = (Utils::expMod(pp,p1,p1*pp) - Utils::expMod(p1,pp-1,p1*pp));

    a = 1;

    do{
        p = p0 + (a*p1*pp);
        //cout << "Test2: " << a2 << endl;
        a++;
    }while(!isPrime(p, iterations));

    return p;
}

bigNum Prime::getStrogPrimeByRSA(int bits, unsigned int iterations)
{
    bigNum p2, p1, p, x, a1, a2, start, end;

    int i1 = 0, i2 = 0, i3 = 0;
    bits = bits >> 1;

    start = NTL::to_ZZ(1) << bits;
    end = NTL::to_ZZ(1) << (bits >> 1);

    x = NTL::to_ZZ(1); a2 = NTL::to_ZZ(2); a1 = NTL::to_ZZ(2);

    do{
        p2 = Random::getRand(start, end);
        p2 = p2 + x % (end-start);
        if (p2 < 0) p2 = p2 * -1;
        x++;
        i1++;
        //cout << "p2: " << p2 << endl;
    }while(!isPrime(p2, iterations));

    //cout << "Intentos p--: " << i1 << endl;

    a2 = Random::getRand(start, end);
    //a2 = 2;
    if(a2%2 == 1)a2++;
    do{
    //    cout << "A2: " << a2 << endl;
        p1 = p2*a2 + 1;
        i2++;
        a2 += 2;
    }while(!isPrime(p1, iterations));
    //cout << "Intentos p-: " << i2 << endl;
    //a1 = 2;
    a1 = Random::getRand(start, end);
    if(a1%2 == 1)a1++;
    do{
        p = p1*a1 +1;
    //    cout << "A1: " << a1 << endl;
        i3++;
        a1 += 2;
    }while(!isPrime(p, iterations));
    //cout << "Intentos p: " << i3 << endl;
    //cout << "Intentos para generar el primo fuerte: " << i1+i2+i3 << endl;
    return p;
}
