#include "affine.h"

Affine::Affine(bigNum a, bigNum b, string alphabet)
{
  this->a = a;
  this->b = b;
  this->alphabet = alphabet;
}

string Affine::encrypt(string M)
{
  for(int i=0;i<M.size();i++)
  {
    M[i] = alphabet[mod((a * alphabet.find(M[i]) + b), alphabet.size())];
  }

  return M;
}

string Affine::decrypt(string M)
{
  bigNum inv = Utils::inverse(a, NTL::to_ZZ(alphabet.size()));
  //cout << endl << inv << endl;
  for(int i=0;i<M.size();i++)
  {
     M[i] = alphabet[mod((inv * (alphabet.find(M[i]) - b) ), alphabet.size())];
  }

  return M;
}

int Affine::mod(bigNum m, int n)
{
  int r;
  if(m < 0){
      r = NTL::to_int(m + n *(((-1*m)/n)+1));
      return r;
  }
  r = NTL::to_int(m-n*(m/n));
  return r;
}
