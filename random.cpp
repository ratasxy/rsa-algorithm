#include "random.h"

Random::Random()
{
}

int Random::getSalt()
{
    fstream fs;
    fs.open("/dev/urandom", ios::in|ios::binary);
    int a = (int) fs.get();
    fs.close();

    return a;
}

bigNum Random::getRand(bigNum start, bigNum end)
{
    srand(getSalt());
    //srand(cryptoSysSalt());
    bigNum x = NTL::to_ZZ(rand()) % (end - start) + start;
    return x;
}

int Random::cryptoSysSalt()
{
    int nbytes = 8;
    string buf;
    static unsigned int seed;
    int i;

    if (seed == 0)
    {
        seed = (unsigned)time(NULL) ^ (unsigned)clock();
        srand(seed);
    }
    for (i = 0; i < nbytes; i++)buf += rand() & 0xFF;

    const char * a = buf.c_str();

    int b = *((int*)(a));

    return b;
}
