#ifndef PRIME_H
#define PRIME_H

#include "utils.h"
#include "random.h"

class Prime
{
public:
    Prime();
    static bool isPrime(bigNum, unsigned int);
    static bigNum getPrime(bigNum, bigNum, unsigned int);
    static bigNum getStrogPrimeByRSA(int, unsigned int);
    static bigNum getStrogPrimeByWilliamsSchmid(int, unsigned int);
    static bigNum getStrogPrimeByGordon(int, unsigned int);
    static bigNum getPrimeNotStrong(int, unsigned int);
};

#endif // PRIME_H
