#include "rsa.h"
#include "benchmark.h"

using namespace std;
int mainBenchmarks(){
    Benchmark *notStrong;
    Benchmark *rsaPrime;
    Benchmark *william;
    Benchmark *gordon;

    double nS, rP, wi, go;

    for(int i=64;i<2049;i*=2)
    {
        cout << endl << "Benchmark a " << i << " bits:" << endl << endl;
        notStrong = new Benchmark(Prime::getPrimeNotStrong, i, 80, 1);
        rsaPrime = new Benchmark(Prime::getStrogPrimeByRSA, i, 80, 1);
        william = new Benchmark(Prime::getStrogPrimeByWilliamsSchmid, i, 80, 1);
        gordon = new Benchmark(Prime::getStrogPrimeByGordon, i, 80, 1);

        nS = notStrong->exec();
        rP = rsaPrime->exec();
        wi = william->exec();
        go = gordon->exec();

        cout << "Not strong Average to " << i << "bits \t " << " " << nS <<endl;
        cout << "RSA METHOD Average to " << i << "bits \t " << " " << rP <<endl;
        cout << "William Sc Average to " << i << "bits \t " << " " << wi <<endl;
        cout << "GORDON Me. Average to " << i << "bits \t " << " " << go <<endl;
    }
}
