#include "rsa.h"
#include "factorization.h"
#include "benchmark.h"
#include "file.h"
#include "digitalsignature.h"

int main()
{
    DigitalSignature ds(16);
    RSA rsa(NTL::to_ZZ("114813069527425452423283320117768198402231770208869520047764273682576626139237031385665948631650626991844596463898746277344711896086305533142593135616525019890688252990321769648847655716265948807907603674767183052682451608244135075132688702494126476924292394930980648739347451409857431810519464366385395918787697992977108407168991243022435383600282017415689609631435016839173015681909866639865997034169693628649785255892275951071625561705105270540841495904010012585689975761105992552407499811822533316628821979845934723925158293235517321794446067477754701469951839802364430167413307649584511999026618861"), NTL::to_ZZ("48299615110782409964817198565382104022210811510440948413284346991635114089442795143295439904900110567236752734984365060574138978285926137182725587881985852679359329414219108358856862314384746289660788208939938680871510620739017181229143279882714631487975879457451892212333935507049688142458791886439251699549087480946145094282895612205417991482888429569165491185704739716383154726677667004331289746892289703827377430592218645521582312891639359140994479784061786629889465845427477260182248761616918669699369749248077667260502459706508484530355658604287027375176845816326581472407377283018635761512125097"), NTL::to_ZZ("10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435998069596029187157175551109609256238876904138374790505436788622230509600358957832936075425976919344334558893888876986294861605897679230656413340841791886021817025"));
    string salir = "nnnnn";
    string cmd = "nnnn";
    string t;
    do{
        cout << "Ingresa acción e, d, -: " << endl;
        cin >> cmd;
        if(cmd == "e"){
            cout << "Encriptar imagen (i) o texto (t) ? " << endl;
            cin >> cmd;
            cout << "Con tu clave privada (t) o la de otros (o)?" << endl;
            cin >> t;
            string n="0", e="0", m="0";
            m = "En un reporte la consultora explico que el bajo resultado del PBI en el cuarto mes del anio respondio a una retraccion de la produccion minera en 96 y a la caida del sector construccion por un descenso de la inversion publica en 15 Sin embargo Apoyo Consultoria dijo que en constraste al declive de los sectores mencionados se observo en abril algunas seniales de estabilizacion o ligera mejora en mercados vinculados al gasto privado En esa linea estima que la economia peruana tendra un mejor desempenio en el segundo semestre del anio ";
            if(t == "o")
            {

            //m = "Lorem ipsum dolor sit amet consectetur adipiscing elit Donec euismod justo massa eu feugiat nulla porttitor et Praesent at tortor dui Sed suscipit vitae mauris at dictum Aenean interdum interdum mi varius porttitor eros rhoncus sit amet Proin porta lectus neque et commodo risus blandit eu Vestibulum malesuada erat lorem in lacinia ipsum consectetur sit amet Curabitur faucibus tortor dolor et tincidunt justo pharetra et Phasellus erat metus facilisis quis ultrices at placerat nec sapien Vivamus ornare mauris a luctus adipiscing lorem lorem egestas ante vitae porttitor justo nibh eu tellus Donec sed ipsum quam Sed commodo eget lacus nec bibendum Aenean aliquet leo est eu elementum lacus ullamcorper quis Donec adipiscing diam in erat eleifend in semper nisi tempus Vestibulum imperdiet orci ac convallis pulvinar velit turpis consequat elit nec faucibus orci nibh in quam";
            cout << "Ingresa n: " << endl;
            cin >> n;
            cout << "Ingresa e: " << endl;
            cin >> e;
            }
            RSA rtm(NTL::to_ZZ(n.c_str()),NTL::to_ZZ("0"), NTL::to_ZZ(e.c_str()));
            if(cmd == "t" ){
                if(t == "o"){
                    cout << rtm.encrypt(m, 0) << endl;
                }else{
                    cout << rsa.encrypt(m, 0) << endl;
                }
            }else{
                File img("/home/ratasxy/image.png", 250, 250, 0);
                if(t == "o"){
                    cout << "Encriptando Imagen con la clave de otro" << endl;
                    rtm.encrypt(img);
                }else{
                    cout << "Encriptando Imagen con tu clave privada" << endl;
                    rsa.encrypt(img);
                }
            }

        }else if(cmd == "d"){
            cout << "Desencriptar imagen (i) o (t) " << endl;
            cin >> cmd;
            if(cmd == "t"){
                cin >> cmd;
                cout << rsa.decrypt(cmd, 0) << endl;
            }else{
                File img("encrypted.bmp", 250, 250, 1);
                rsa.decrypt(img);
            }
        }
    }while(cmd != "-");
}
