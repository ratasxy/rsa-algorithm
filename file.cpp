#include "file.h"

File::File(string path, int x, int y, bool isEncrypted)
{
    QString fileName(path.c_str());

    QImage ** i = (isEncrypted) ? &iEnc : &image;
    (*i) = new QImage(fileName);
    if ((*i)->isNull()) {
        cout << "Error al cargar imagen" << endl;
        return;
    }

    this->x = x;
    this->y = y;
}

void File::encodePixels()
{
    string txt;
    //cout << "X: " << this->iEnc->width() << " Y:" << this->iEnc->height() << endl;

    for(int x=0;x<this->x;x++)
    {
        for(int y=0;y<this->y;y++)
        {
            QColor ctmp = image->pixel(QPoint(x,y));
            txt += Utils::encodeRGB(ctmp.red(), ctmp.green(), ctmp.blue());
        }
    }

    this->encoded = txt;
}

void File::decodePixels()
{
    string txt;
    int tx=this->iEnc->width(), ty=this->iEnc->height();
    cout << "X: " << this->iEnc->width() << " Y:" << this->iEnc->height() << endl;
    //cout << "Decodificando" << endl;
    for(int x=0;x<tx;x++)
    {
        for(int y=0;y<ty;y++)
        {
            QColor ctmp = iEnc->pixel(QPoint(x,y));
            txt += Utils::inverseEncodeRGB(ctmp.red(), ctmp.green(), ctmp.blue());
        }
    }
    //cout << txt << endl;
    this->decoded = txt;
}

void File::saveEncryptedImage()
{
    int npixeles = (this->encrypted.size()+5)/6;

    int rows = this->x;
    int cols = (npixeles+(rows-1))/rows;
    int arow = 0, acol = 0;

    this->iEnc = new QImage(rows,cols,QImage::Format_RGB32);
    for(int i=0;i<this->encrypted.size();i+=6){
        string subTmp = this->encrypted.substr(i, 6);
        //cout << subTmp << endl;
        subTmp = Utils::addZeros(subTmp, 6,0);
        int r[3];
        for(int j=0;j<6;j+=2){

            r[j/2] = NTL::to_int(Utils::to_num(subTmp.substr(j,2)));
        }
        QRgb pc;

        pc = qRgb(r[0],r[1],r[2]);

        //cout << r[0] << r[1] << r[2] << endl;
        this->iEnc->setPixel(QPoint(arow, acol),pc);
        //cout << pc << endl;
        //cout << pc.red() << pc.green() << pc.blue() << endl;
        //cout << "--------" << endl;
        //return;
        acol++;
        if((acol % cols==0))
        {
            arow++;
            acol = 0;
        }
    }
    this->iEnc->save("encrypted.bmp", "BMP");
}

void File::saveDecryptedImage()
{
    //int npixeles = (this->encrypted.size()+5)/6;

    int rows = this->x;
    int cols = this->y;
    //int cols = (npixeles+(rows-1))/rows;
    int arow = 0, acol = 0;

    this->iEnc = new QImage(rows,cols,QImage::Format_RGB32);
    for(int i=0;i<this->decrypted.size();i+=9){
        string subTmp = this->decrypted.substr(i, 9);
        cout << subTmp << endl;
        subTmp = Utils::addZeros(subTmp, 9, 1);
        int r[3];
        for(int j=0;j<9;j+=3){
            r[j/3] = NTL::to_int(Utils::to_num(subTmp.substr(j,3)));
        }
        QRgb pc;

        pc = qRgb(r[0],r[1],r[2]);

        this->iEnc->setPixel(QPoint(arow, acol),pc);
        //cout << pc << endl;
        acol++;
        if((acol % cols==0))
        {
            arow++;
            acol = 0;
        }
        if(arow == rows)break;
    }
    this->iEnc->save("original.bmp", "BMP");
}
