#include "rsa.h"

RSA::RSA(bigNum p, bigNum q, bigNum privateKey, bigNum publicKey)
{
    this->p = p;
    this->q = q;
    this->N = p * q;
    this->privateKey = privateKey;
    this->publicKey = publicKey;
    this->alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

}

RSA::RSA(bigNum n, bigNum privateKey, bigNum publicKey)
{
    this->N = n;
    this->privateKey = privateKey;
    this->publicKey = publicKey;
    this->alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

}

RSA::RSA(int bits)
{
    this->start = NTL::to_ZZ(1) << bits;
    this->end = NTL::to_ZZ(1) << (bits >> 1);
    this->iterations = 50;
    this->nb = bits;
    this->alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    //this->alphabet = "abcdefghijklmnopqrstuvwxyz ";
    cout << "Generating keys: ";
    this->generate_keys();
    cout << "DONE!" << endl;

    cout << "PrivateKey\t\t: " << this->privateKey << endl;
    cout << "PublicKey\t\t: " << this->publicKey << endl;

    cout << "P\t\t: " << this->p << endl;
    cout << "Q\t\t: " << this->q << endl;
    cout << "N\t\t: " << this->N << endl;
}

void RSA::generate_keys()
{
    bigNum p, q;

    p = Prime::getPrime(start, end, iterations);
    do{
        q = Prime::getPrime(start, end, iterations);
    }while(p == q);

    this->p = p;
    this->q = q;

    this->N = p*q;
    bigNum ON = (p-1)*(q-1);

    bigNum e;
    do{
        //e = Random::getRand(start, end);
        e = Random::getRand(start, end);
        e *= e;
    }while(Utils::gcd(ON, e) != 1);

    this->publicKey = e;

    bigNum ie = Utils::inverse(this->publicKey, ON);
    this->privateKey = ie % ON;
}

bigNum RSA::encryptNumber(bigNum m)
{
    return Utils::expMod(m,this->publicKey,this->N);
}

bigNum RSA::decryptNumber(bigNum c)
{
    return Utils::expMod(c,this->privateKey,this->N);
}

bigNum RSA::decryptNumberChinese(bigNum c)
{
    bigNum b1, b2, a1, a2;

    b1 = this->privateKey % (this->p - 1);
    b2 = this->privateKey % (this->q - 1);

    a1 = Utils::expMod(c, b1, this->p);
    a2 = Utils::expMod(c, b2, this->q);

    return Utils::chineseRemainder(a1, this->p, a2, this->q);
}

string RSA::encryptMessage(string M)
{
    for(long long int i=0; i < M.size(); i++)
    {
        M[i] = alphabet[encryptNumber(NTL::to_ZZ(alphabet.find(M[i]))) % alphabet.size()];
    }
    return M;
}

string RSA::encrypt(File &file)
{
    file.encodePixels();
    file.encrypted = this->encrypt(file.encoded, 1);
    file.saveEncryptedImage();
    return file.encrypted;
}

string RSA::decrypt(File &file)
{
    file.decodePixels();
    file.decrypted = this->decrypt(file.decoded, 1);
    //cout << file.decrypted << endl;
    file.saveDecryptedImage();
    return file.decrypted;
}

string RSA::encrypt(string message, bool isFile)
{
    vector<string> result;
    vector<string> encryptedMessage;
    string text;
    if(!isFile){
    result = Utils::splitStrings(message, this->alphabet);
        text = Utils::join(result);
    }else{
        text = message;
    }

    int sizeOfN = Utils::to_string(this->N).size();
    cout << endl;
    for(unsigned long long i = 0; i < text.size(); i+=(sizeOfN-1)){
        string txt = Utils::addZeros(text.substr(i, (sizeOfN-1)), sizeOfN-1, true);
        //cout << "D: " << txt << endl;
        bigNum m = this->encryptNumber(Utils::to_num(txt));
        encryptedMessage.push_back(Utils::addZeros(Utils::to_string(m), sizeOfN));
        //cout << "E: " << Utils::addZeros(Utils::to_string(m), sizeOfN) << endl;
    }

    text = Utils::join(encryptedMessage);
    return text;
}

string RSA::decrypt(string message, bool isFile)
{
    int sizeOfN = Utils::to_string(this->N).size();

    vector<string> decryptedMessage;
    cout << endl;
    for(unsigned int i = 0; i < message.size(); i+=(sizeOfN)){
        //cout << "E: " << message.substr(i, sizeOfN) << endl;
        bigNum m = this->decryptNumber(Utils::to_num(message.substr(i, sizeOfN)));
        //bigNum m = this->decryptNumberChinese(Utils::to_num(message.substr(i, sizeOfN)));

        string textTmp = Utils::to_string(m);
        //cout << "D: " << textTmp << endl;
        //cout << "****" << endl;
        decryptedMessage.push_back(Utils::addZeros(textTmp, sizeOfN-1));
    }

    cout << "Termine esto" << endl;
    string text = Utils::join(decryptedMessage);

    string a;
    //cout << "Mensaje: " << text << endl;
    int size = Utils::to_string(NTL::to_ZZ(this->alphabet.size())).size();
    for(unsigned int i = 0; i < text.size(); i+=size) {
        int n = NTL::to_int(Utils::to_num(text.substr(i, size)));
        a += this->alphabet[n];
    }

    if(isFile){
        a = text;
    }

    return a;
}

string RSA::decryptMessage(string C)
{
    for(long long int i=0; i < C.size(); i++)
    {
        C[i] = alphabet[decryptNumber(NTL::to_ZZ(alphabet.find(C[i]))) % alphabet.size()];
    }
    return C;
}
